﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BloggingSystemApp.Startup))]
namespace BloggingSystemApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
