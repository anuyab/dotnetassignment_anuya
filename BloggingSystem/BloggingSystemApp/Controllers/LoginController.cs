﻿using BloggingSystemApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BloggingSystemApp.Controllers
{
    public class LoginController : Controller
    {
        private BloggingAppDbContext db = new BloggingAppDbContext();
        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn([Bind(Include = "UserName, Password")]Blog_User user)
        {
            Blog_User validUser = null;
            if (ModelState.IsValid)
            {
                validUser = db.Blog_User.SingleOrDefault(usr => (usr.UserName == user.UserName && usr.Password == user.Password));
            }
            if (validUser != null)
            {
                Session["LoggedUser"] = validUser;
                Session["UserName"] = "Welcome, " + validUser.Name;

                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.LoginFailedMessage = "Login Failed! Please Try again";
                return View("Index");
            }

        }

        public ActionResult Logout()
        {
            Session["LoggedUser"] = null;
            Session["UserName"] = null;
            return RedirectToAction("Index", "Home");
        }

        public JsonResult IsUserAvailable(String UserName)
        {
            return Json(db.Blog_User.Any(usr => usr.UserName == UserName), JsonRequestBehavior.AllowGet);
        }
    }
}