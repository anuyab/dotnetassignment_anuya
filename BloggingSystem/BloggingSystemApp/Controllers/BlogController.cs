﻿using BloggingSystemApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BloggingSystemApp.Controllers
{
    public class BlogController : Controller
    {
        private BloggingAppDbContext db = new BloggingAppDbContext();
        // GET: Blog
        public ActionResult Index()
        {
            var blog_Posts = db.Blog_Posts.Include(b => b.Blog_Categories).Include(b => b.Blog_User);
            return View(blog_Posts.ToList());
        }
        public ActionResult MyBlogList()
        {
            Blog_User Loggeduser = (Blog_User)Session["LoggedUser"];
            try
            {
                List<Blog_Posts> MyPostList = db.Blog_Posts.Where(post => post.PostUserID == Loggeduser.UserID).ToList();
                return View("MyBlogList", MyPostList);
            }
            catch
            {
                ViewBag.NoPostsMessage = "Please add the posts";
                return View();
            }

        }

        // GET: Blog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog_Posts blog_Posts = db.Blog_Posts.Find(id);
            if (blog_Posts == null)
            {
                return HttpNotFound();
            }
            return View(blog_Posts);
        }

        // GET: Blog_Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog_Posts blog_Posts = db.Blog_Posts.Find(id);
            if (blog_Posts == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostCategoryID = new SelectList(db.Blog_Categories, "CategoryID", "CategoryName", blog_Posts.PostCategoryID);
            ViewBag.PostUserID = new SelectList(db.Blog_User, "UserID", "UserName", blog_Posts.PostUserID);
            return View(blog_Posts);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PostID,Title,PostContent")] Blog_Posts blog_Posts)
        {
            Blog_Posts EditPost = db.Blog_Posts.SingleOrDefault(post => post.PostID == blog_Posts.PostID);
            EditPost.PostContent = blog_Posts.PostContent;
            EditPost.Title = blog_Posts.Title;
            EditPost.DateModified = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(EditPost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MyBlogList");
            }
            ViewBag.PostCategoryID = new SelectList(db.Blog_Categories, "CategoryID", "CategoryName", blog_Posts.PostCategoryID);
            ViewBag.PostUserID = new SelectList(db.Blog_User, "UserID", "UserName", blog_Posts.PostUserID);
            return View(blog_Posts);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.PostCategoryID = new SelectList(db.Blog_Categories, "CategoryID", "CategoryName");
            ViewBag.PostUserID = new SelectList(db.Blog_User, "UserID", "UserName");
            return View();
        }

        // POST: Blog_Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title,PostContent,PostCategoryID")] Blog_Posts blog_Posts)
        {
            if (ModelState.IsValid)
            {
                Blog_User user = (Blog_User)Session["LoggedUser"];
                blog_Posts.PostUserID = user.UserID;
                blog_Posts.Author = user.UserName;
                blog_Posts.DateCreated = DateTime.Now;
                blog_Posts.DateModified = DateTime.Now;
                blog_Posts.IsPublished = true;
                blog_Posts.Slug = blog_Posts.Title.Replace(" ", "_") + "_by_" + user.Name;
                db.Blog_Posts.Add(blog_Posts);
                db.SaveChanges();
                return RedirectToAction("MyBlogList");
            }

            ViewBag.PostCategoryID = new SelectList(db.Blog_Categories, "CategoryID", "CategoryName", blog_Posts.PostCategoryID);
            ViewBag.PostUserID = new SelectList(db.Blog_User, "UserID", "UserName", blog_Posts.PostUserID);
            return View(blog_Posts);
        }

        public ActionResult Delete(int id)
        {
            Blog_Posts blog_Posts = db.Blog_Posts.Find(id);
            db.Blog_Posts.Remove(blog_Posts);
            db.SaveChanges();
            return RedirectToAction("MyBlogList");
        }
    }
}