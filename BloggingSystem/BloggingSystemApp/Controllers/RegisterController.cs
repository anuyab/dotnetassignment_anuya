﻿using BloggingSystemApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BloggingSystemApp.Controllers
{
    public class RegisterController : Controller
    {
        private BloggingAppDbContext db = new BloggingAppDbContext();
        // GET: Blog_User/Create
        public ActionResult Register()
        {
            ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "RoleName");
            return View();
        }

        // POST: Blog_User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserName,Password,Name")] Blog_User blog_User)
        {
            if (ModelState.IsValid)
            {
                int Userrole = db.Roles.Where(role => role.RoleName == "End Users").Select(role => role.RoleId).ToList()[0];
                blog_User.RoleId = Userrole;
                db.Blog_User.Add(blog_User);
                db.SaveChanges();
                return RedirectToAction("MyBlogList", "Blog");
            }

            ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "RoleName", blog_User.RoleId);
            return View(blog_User);
        }
    }
}